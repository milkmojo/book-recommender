Summary: A webtask.io tool that interfaces with Goodreads API to come up with a book you might want to read.

Currently just a proof of concept. The end state ideally would look as follows:

1. Authenticate a user against Goodreads
2. Retrieve persons of interest from the user's account
3. Aggregate books read by those persons
4. Select a book to recommend
5. Link the user to purchase the book

In addition, allow the user to subscribe to receive a recommendation on some interval. If the user has received a recommendation before, don't present that book as a recommendation again.


