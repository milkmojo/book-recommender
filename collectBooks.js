"use latest";
var rp = require('request-promise');
var Promise = require('bluebird');

// Some user IDs to use for now, but longer term, this tool would be
// authenticated against Goodreads and the users would be able to
// get recommendations based on the users they follow.
var users = [602555,67317968];

module.exports = (ctx,cb) => {
    var calls = [];
    var books = [];
    users.forEach((user) => {
        var options = {
            uri: ctx.secrets.GET_BOOKS_BY_USER_ID_URL,
            qs: {
                userId: `${user}`
            },
            json: true
        };
        calls.push(rp(options).then((booksForUser) => {
            books.push(JSON.parse(booksForUser));
        }));
    });

    Promise.all(calls)
        .then(() => { 
            cb(null, books.reduce((a, b) => a.concat(b), [])); 
        }).catch((err) => { 
            cb(null,err);
        });
};