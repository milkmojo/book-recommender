"use latest";
var request = require('request');

module.exports = (ctx, cb) => {
    var options = {
        uri: ctx.secrets.COLLECT_BOOKS_URL,
        json: true
    };
    request(options, (error, res, body) => {
        if (error) 
            console.log(error);
        else
            //TODO: perform some validation on the body before filtering
            cb(null,filterBooks(body));
    });
};


// Here we can implement a much more robust selection process, but for now
// we'll simply return the highest rated book.
// 
// Other considerations for the future:
//      Rate of occurrence (when multiple users recently read the same book)
//      How recent it was read/updated?
//      Is it in a genre of interest?
//      How highly rated is the author?
function filterBooks(books) {
    books.sort((a,b) => {
        return parseFloat(b.averageRating) - parseFloat(a.averageRating);
    });
    return books[0];
}