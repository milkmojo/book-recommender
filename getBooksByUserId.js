"use latest";
var request = require('request');
var convert = require("xml2js");

module.exports = (ctx,cb) => {
    var options = {
        uri: 'https://www.goodreads.com/review/list',
        method:'GET',
        qs: {
            v: 2,
            id:`${ctx.data.userId}`,
            per_page:10,
            key:`${ctx.secrets.API_KEY}`,
            sort:'date_updated',
            order:'d'
        }
    };
    request(options, (error, res, bodyXml) => {
        if (error) 
            console.log(error);
        else
            convert.parseString(bodyXml,(parseError, parseResult) => {
                if (parseError)
                    console.log(parseError);
                else
                    var bookDetails = getBookDetailsFromResponse(parseResult);
                    cb(null,JSON.stringify(bookDetails));
            });
    });
};

function getBookDetailsFromResponse(obj) {
    var reviews = obj.GoodreadsResponse.reviews[0].review;
    var bookDetails = [];
    reviews.forEach((review) => {
        //return isbns in case we want to do something external with these later
        var bookDetail = {};
        var book = review.book[0];
        if (book['$'])
            return;
        var isbn = book.isbn[0];
        if (isbn['$'])
            return;
        bookDetail.isbn = isbn;
        bookDetail.title = book.title[0];
        bookDetail.averageRating = book.average_rating[0];
        bookDetail.publisher = book.publisher[0];
        bookDetails.push(bookDetail);
    });
    return bookDetails;
}
